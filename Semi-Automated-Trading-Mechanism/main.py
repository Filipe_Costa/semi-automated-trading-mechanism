import tkinter as tk
import logging

from connectors.binance_futures import BinanceFuturesClient
from connectors import config

# Initializes the logger
logger = logging.getLogger()

# Set a minimal logging level of INFO - 
# Implies that only message including info level - logging messages, and above will be displayed on screen 
logger.setLevel(logging.DEBUG)

# The following block adds a stream handler level format to the output messages which will be logged to the console
stream_handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(levelname)s :: %(message)s')
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.INFO)

# Adding a file handler
file_handler = logging.FileHandler('info.log')
file_handler.setFormatter(formatter)
file_handler.setLevel(logging.DEBUG)

# Add the stream handler & file handler to the logger instance
logger.addHandler(stream_handler)
logger.addHandler(file_handler)

# Writes serveral warning messages, for testing purposes
# logger.debug("This message is important only when debugging the program")
# logger.info("This message just shows basic information")
# logger.warning("This message is about something you should pay attention to")
# logger.error("This message helps todebug an error that occurred in the program")

# whats after the if statement will be executed only if the main module is executed
# this means that what's after the if statement won't be executed if the main module is imported to another module
if __name__ == '__main__':

    binance = BinanceFuturesClient(config.futuresApiKey, config.futuresApiSecurity, True)
    print(binance.get_balances())
    print(binance.place_order("BTCUSDT", "BUY", 0.01, "LIMIT", 20000, "GTC"))
    print(binance.get_order_status("BTCUSDT", 2677216816))
    print(binance.cancel_order("BTCUSDT", 2677216816))



    # Blocking function which will prevent the program from terminating
    # Waits for actions from the user, its a window dialog
    root = tk.Tk()
    root.mainloop()

