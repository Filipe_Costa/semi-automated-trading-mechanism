import logging
import requests
import time

from urllib.parse import urlencode

import hmac
import hashlib

import websocket
import json

import threading

logger = logging.getLogger()

# Function to get contracts from binance API
# def get_contracts():

#     # Get general contracts from the binance api, without providing credentials
#     response = requests.get("https://api.binance.com/api/v3/exchangeInfo")
#     print(response.status_code)
#     # Uncomment the line below to view the json structure
#     #pprint.pprint(response.json())

#     contracts = []

#     # Iterate through the json array, and append every symbol to the list of contracts
#     for contract in response.json()['symbols']:
#         contracts.append(contract['symbol'])

#     return contracts

# print(get_contracts())

# Class to trade Binance futures
class BinanceFuturesClient:
    def __init__(self, public_key, secret_key, testnet):
        if testnet:
            self.base_url = "https://testnet.binancefuture.com"
            self.wss_url = "wss://stream.binancefuture.com/ws"
        else:
            self.base_url = "https://api.binance.com"
            self.wss_url = "wss://fstream.binance.com/ws"

        #  Defining the public and private keys
        self.public_key = public_key
        self.secret_key = secret_key

        # Headers dictionary
        self.headers = {'X-MBX-APIKEY': self.public_key}

        # The key of this dictionary will be the contract name, and the value will contain the bid and ask price
        self.prices = dict()

        # Initializing a self.id
        self.id = 1
        self.ws = None

        # Create the thread before running the start_ws function
        thread = threading.Thread(target=self.start_ws)
        thread.start()

        logger.info("Binance Futures client successfully initialized")

    def generate_signature(self, data):
        return hmac.new(self.secret_key.encode(), urlencode(data).encode(), hashlib.sha256).hexdigest()

    # Function to be called when performing requests
    def make_request(self, method, endpoint, data):
        if method == "GET":
            response = requests.get(self.base_url + endpoint, params=data, headers=self.headers)
        elif method == "POST":
            response = requests.post(self.base_url + endpoint, params=data, headers=self.headers)
        elif method == "DELETE":
            response = requests.delete(self.base_url + endpoint, params=data, headers=self.headers)
        else:
            raise ValueError()

        if response.status_code == 200:
            return response.json()
        else:
            logger.error("Error while making %s request to %s: %s (error code %s)", method, endpoint, response.json(), response.status_code)
            return None

    # Function to get contracts
    def get_contracts(self):
        # Arrange
        exchange_info = self.make_request("GET", "/fapi/v1/exchangeInfo", None)

        contracts = dict()

        # Act
        if exchange_info is not None:
            for contract_data in exchange_info['symbols']:
                contracts[contract_data['pair']] = contract_data

        # Assert
        return contracts

    # Get the history of the market transactions, by retrieving the candlesticks
    def get_historical_candles(self, symbol, interval):
        # Arrange
        data = dict()
        data['symbol'] = symbol
        data['interval'] = interval
        data['limit'] = 1000

        
        raw_candles = self.make_request("GET", "/fapi/v1/klines", data)

        candles = []

        # Act
        if raw_candles is not None:
            for candle in raw_candles:
                candles.append([candle[0], float(candle[1]), float(candle[2]), float(candle[3]), float(candle[4]), float(candle[5])])
        
        # Assert
        return candles

    # Function to get order book price for a specific symbol
    def get_bid_ask(self, symbol):
        # dictionary initialization and key/pair creation
        # Arrange
        data = dict()
        data['symbol'] = symbol

        # declaration of the order book data object containing the json data brought by the request
        
        ob_data = self.make_request("GET", "/fapi/v1/ticker/bookTicker", data)

        # Act
        if ob_data is not None:
            # if the symbol doesn't match symbols present in the dictionary, place the data on the diction
            if symbol not in self.prices:
                # This data can be viewed on the json response example at "https://binance-docs.github.io/apidocs/futures/en/#symbol-order-book-ticker"
                # I'm interested in the bidPrice and askPrice variables, which will be converted and put into the dictionary on the lines below
                self.prices[symbol] = {'bid': float(ob_data['bidPrice']), 'ask': float(ob_data['askPrice'])}
            # otherwise update the keys to the contract I already have
            else:
                self.prices[symbol]['bid'] = float(ob_data['bidPrice'])
                self.prices[symbol]['ask'] = float(ob_data['askPrice'])

        # Assert
        return self.prices[symbol]

    # Retrieves available balance from the platform
    def get_balances(self):
        # This dictionary needs the timestamp and the signature which is generated by the function generate_signature() declared above
        # Arrange
        data = dict()
        data['timestamp'] = int(time.time() * 1000)
        data['signature'] = self.generate_signature(data)

        # This dictionary will contain the key pair values used to retrieve information from the exchange
        balances = dict()

        # The account data variable is used to store the response from the request
        
        account_data = self.make_request("GET", "/fapi/v2/account", data)

        # The if condition iterates through every asset and stores the asset's balance in the balances dictionary created above
        # Act
        if account_data is not None:
            for asset in account_data['assets']:
                balances[asset['asset']] = asset

        # Assert
        return balances

    def place_order(self, symbol, side, quantity, order_type, price=None, tif=None):
        # Arrange
        data = dict()
        data['symbol'] = symbol
        data['side'] = side
        data['quantity'] = quantity
        data['type'] = order_type

        if price is not None:
            data['price'] = price

        if tif is not None:
            data['timeInForce'] = tif

        data['timestamp'] = int(time.time() * 1000)
        data['signature'] = self.generate_signature(data)

        # Act
        order_status = self.make_request("POST", "/fapi/v1/order", data)

        # Assert
        return order_status

    def cancel_order(self, symbol, order_id):
        # Arrange
        data = dict()
        data['orderId'] = order_id
        data['symbol'] = symbol

        data['timestamp'] = int(time.time() * 1000)
        data['signature'] = self.generate_signature(data)

        # Act
        order_status = self.make_request("DELETE", "/fapi/v1/order", data)
        
        # Assert
        return order_status

    def get_order_status(self, symbol, order_id):
        # Arrange
        data = dict()
        data['timestamp'] = int(time.time() * 1000)
        data['symbol'] = symbol
        data['orderId'] = order_id
        data['signature'] = self.generate_signature(data)

        # Act
        order_status = self.make_request("GET", "/fapi/v1/order", data)

        # Assert
        return order_status
    
    def start_ws(self):
        # Open a websocket connection
        self.ws = websocket.WebSocketApp(self.wss_url, on_open=self.on_open, on_close=self.on_close, on_error=self.on_error)
        
        # Run the connection - IMPORTANT - since this function is an infinite loop, it must run on a thread 
        self.ws.run_forever()

    def on_open(self, ws):
        logger.info("Binance websocket connection opened")

        # When the connection opens we directly subscribe to a channel
        self.subscribe_channel("BTCUSDT")

    def on_close(self, ws):
        logger.warning("Binance websocket connection closed")

    def on_error(self, ws, msg):
        logger.error("Binance connection error: %s", msg)

    def on_message(self, ws, msg):
        # Convert the json string that was received to a json object which can be parsed
        data = json.loads(msg)

        # Check that the "e" is in the dictionary
        if "e" in data:
            if data['e'] == "bookTicker":
                # Symbol is represented by 's'
                symbol = data['s']
                if symbol not in self.prices:
                    # This data can be viewed on the json response example at "https://binance-docs.github.io/apidocs/futures/en/#symbol-order-book-ticker"
                    # I'm interested in the bidPrice and askPrice variables, which will be converted and put into the dictionary on the lines below
                    self.prices[symbol] = {'bid': float(data['b']), 'ask': float(data['a'])}
                # otherwise update the keys to the contract I already have
                else:
                    self.prices[symbol]['bid'] = float(data['b'])
                    self.prices[symbol]['ask'] = float(data['a'])

                print(self.prices(symbol))

    def subscribe_channel(self, symbol):
        data = dict()
        data['method'] = "SUBSCRIBE"
        data['params'] = []
        data['params'].append(symbol.lower() + "@bookTicker")
        data['id'] = self.id

        self.ws.send(json.dumps(data)) 

        # Every time the subscribe_channel function is called, a value is incremented to the id
        self.id += 1

